package com.sample.realtime;

import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.sample.realtime.databinding.ActivityViewJobBinding;

public class ViewJob extends AppCompatActivity {
    private ActivityViewJobBinding binding;
    private DatabaseReference ref;
    private ValueEventListener listener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            JobModel model = null;
            if (dataSnapshot.getValue() != null) {
                model = dataSnapshot.getValue(JobModel.class);
            }

            // set this model to data binding to display the job details
            if (binding != null)
                binding.setJob(model);
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_view_job);

        String jobId = "";
        if (getIntent() != null) {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                jobId = bundle.getString("jobId", "");
            }
        }

        if (!TextUtils.isEmpty(jobId)) {
            // get the Job details
            ref = BaseFirebase.getRef().child(jobId);
            ref.addValueEventListener(listener);
        }
    }

    @Override
    protected void onDestroy() {
        if (ref != null && listener != null)
            ref.removeEventListener(listener);
        super.onDestroy();
    }
}
