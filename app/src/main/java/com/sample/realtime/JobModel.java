package com.sample.realtime;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

public class JobModel {
    private String jobId;
    private String clientName;
    private String clientPhone;
    private String jobNumber;
    private String jobDate;
    private String jobRef;
    private String jobInfo;
    private String paymentConditions;
    private String guarantee;
    private String services;
    private String materials;
    private String discount;

    private long createdAt;

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientPhone() {
        return clientPhone;
    }

    public void setClientPhone(String clientPhone) {
        this.clientPhone = clientPhone;
    }

    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public String getJobDate() {
        return jobDate;
    }

    public void setJobDate(String jobDate) {
        this.jobDate = jobDate;
    }

    public String getJobRef() {
        return jobRef;
    }

    public void setJobRef(String jobRef) {
        this.jobRef = jobRef;
    }

    public String getJobInfo() {
        return jobInfo;
    }

    public void setJobInfo(String jobInfo) {
        this.jobInfo = jobInfo;
    }

    public String getPaymentConditions() {
        return paymentConditions;
    }

    public void setPaymentConditions(String paymentConditions) {
        this.paymentConditions = paymentConditions;
    }

    public String getGuarantee() {
        return guarantee;
    }

    public void setGuarantee(String guarantee) {
        this.guarantee = guarantee;
    }

    public String getServices() {
        return services;
    }

    public void setServices(String services) {
        this.services = services;
    }

    public String getMaterials() {
        return materials;
    }

    public void setMaterials(String materials) {
        this.materials = materials;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public long getCreatedAt() {
        if (createdAt <= 0)
            createdAt = System.currentTimeMillis();

        return createdAt;
    }

    @Exclude
    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("jobId", getJobId());
        map.put("clientName", getClientName());
        map.put("clientPhone", getClientPhone());
        map.put("jobNumber", getJobNumber());
        map.put("jobDate", getJobDate());
        map.put("jobRef", getJobRef());
        map.put("jobInfo", getJobInfo());
        map.put("paymentConditions", getPaymentConditions());
        map.put("guarantee", getGuarantee());
        map.put("services", getServices());
        map.put("materials", getMaterials());
        map.put("discount", getDiscount());
        map.put("createdAt", getCreatedAt());

        return map;
    }
}
