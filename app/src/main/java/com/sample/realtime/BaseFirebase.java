package com.sample.realtime;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Logger;

public class BaseFirebase {
    private static FirebaseDatabase database;

    private BaseFirebase() {

    }

    /**
     * get the instance of realtime database
     */
    public static FirebaseDatabase getDatabase() {
        if (database == null) {
            database = FirebaseDatabase.getInstance();
            database.setPersistenceEnabled(true);
            database.setLogLevel(Logger.Level.DEBUG);
        }
        return database;
    }

    /**
     * get the database ref
     */
    public static DatabaseReference getRef() {
        return getDatabase().getReference()
                .child("jobs");
    }
}
