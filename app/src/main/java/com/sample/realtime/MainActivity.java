package com.sample.realtime;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.sample.realtime.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        JobModel jobModel = new JobModel();
        binding.setJob(jobModel);

        binding.saveMap.setOnClickListener(this);
        binding.saveModel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.saveMap:
                JobModel job = binding.getJob();
                // set the new job id
                job.setJobId(getJobId());
                BaseFirebase.getRef().child(job.getJobId()).updateChildren(job.toMap());
                viewJobDetails(job.getJobId());
                break;
            case R.id.saveModel:
                JobModel jobModel = binding.getJob();
                // set the new job id
                jobModel.setJobId(getJobId());
                BaseFirebase.getRef().child(jobModel.getJobId()).setValue(jobModel);
                viewJobDetails(jobModel.getJobId());
                break;
        }
    }

    /**
     * get the new job id
     */
    private String getJobId() {
        return BaseFirebase.getRef().push().getKey();
    }

    /**
     * view job details
     */
    private void viewJobDetails(String jobId) {
        Intent intent = new Intent(this, ViewJob.class);
        intent.putExtra("jobId", jobId);
        startActivity(intent);
    }
}
