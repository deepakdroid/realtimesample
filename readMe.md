## Realtime Sample

# Steps to produce the issue
1. Turn off the wifi and mobile data on the device/emulator
2. Run the app
3. Fill the data on the Screen
4. Click on save with model. By clicking on this, the data will be saved and you will see the saved data on the next screen because of value event listener and offline persistence.
5. Click on save with Map. By clicking on this, the data will be saved but you will not see the save data on the next screen even with persistence enabled. Which means ValueEventListener is not getting called when saving the data with map. This is the issue.